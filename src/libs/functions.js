import {h} from "vue";

export function bounds(el) {
    const bounds = el.value.getBoundingClientRect()
    let element = el.value
    let _x = 0
    let _y = 0
    while( element && !isNaN( element.offsetLeft ) && !isNaN( element.offsetTop ) ) {
        _x += element.offsetLeft - element.scrollLeft
        _y += element.offsetTop - element.scrollTop
        element = element.offsetParent
    }
    return { top: _y, left: _x, x:bounds.x, y: bounds.y, width:bounds.width, height:bounds.height }
}

export function uuid4() {
    function generateNumber(limit) {
        const value = limit * Math.random()
        return value | 0
    }
    function generateX() {
        const value = generateNumber(16)
        return value.toString(16)
    }
    function generateXes(count) {
        let result = ''
        for(let i = 0; i < count; ++i) {
            result += generateX()
        }
        return result
    }
    function generateVariant() {
        const value = generateNumber(16)
        let variant =  (value & 0x3) | 0x8
        return variant.toString(16)
    }

    return generateXes(8)
        + '-' + generateXes(4)
        + '-' + '4' + generateXes(3)
        + '-' + generateVariant() + generateXes(3)
        + '-' + generateXes(12)
}

export function arraysEqual(a, b) {
    return Array.isArray(a) &&
        Array.isArray(b) &&
        a.length === b.length &&
        a.every((val, index) => val === b[index]);
}

export function isVueComponent(comp) {
    const vnode = h(comp)

    if (!vnode.type) {
        return false
    }

    if (typeof vnode.type === 'string') {
        return false
    }

    if (vnode.type.setup || vnode.type.render) {
        return true
    }

    if (vnode.type.emits || vnode.type.props) {
        return true
    }
}

export function validateField() {

}