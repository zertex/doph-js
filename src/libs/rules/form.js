export default class DForm {
    constructor(fields) {
        this.fields = fields
    }

    validate() {
        let error = false
        for (let field in this.fields) {
            if (this.fields[field].ref.value.validate() === true) {
                error = true
            }
        }
        return !error
    }
}
