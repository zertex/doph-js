class RulesMax {
    validate(str = '', max = 0) {
        return str.length <= max;
    }

    message(text) {
        return text
    }
}

export default new RulesMax()