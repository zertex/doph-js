class RulesUrl {
    validate(str, scheme = 'https') {
        if (!this._isValidURL(str)) {
            str = scheme + '://' + str;
        }
        return this._isValidURL(str);
    }

    message(type, text) {
        let message = ''
        switch (type) {
            case 'url':
                message = text
                break

            default:
                break
        }
        return message
    }

    _isValidURL(str) {
        try {
            new URL(str);
            return true;
        } catch (err) {
            return false;
        }
    }
}

export default new RulesUrl()
