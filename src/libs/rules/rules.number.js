class RulesNumber {
    validate(num) {
        return !isNaN(parseFloat(num)) && !isNaN(num - 0)
    }

    message(text) {
        return text
    }
}

export default new RulesNumber()
