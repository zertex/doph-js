class RulesEmail {
    validate(email) {
        const regex = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
        return regex.test(email);
    }

    message(text) {
        return text
    }
}

export default new RulesEmail()
