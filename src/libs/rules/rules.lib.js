import Rules from "@/libs/rules/rules.js";

class RulesLib {
    validate(value, rules) {
        let error = '';
        if (rules) {
            let hasErrors = false
            for (let rule in rules) {
                if (hasErrors) {
                    continue
                }
                switch (rule) {
                    case 'url':
                        error = Rules.RulesUrl.validate(value, 'https') || value.length === 0 ? '' : Rules.RulesUrl.message('url', rules[rule]?.message ? rules[rule]?.message : 'url_incorrect')
                        hasErrors = error !== ''
                        break
                    case 'required':
                        error = Rules.RulesRequired.validate(value) ? '' : Rules.RulesRequired.message(rules[rule]?.message ? rules[rule]?.message : 'required')
                        hasErrors = error !== ''
                        break
                    case 'min':
                        error = Rules.RulesMin.validate(value, rules[rule]?.value) ? '' : Rules.RulesMin.message(rules[rule]?.message ? rules[rule]?.message : 'very_small')
                        hasErrors = error !== ''
                        break
                    case 'max':
                        error = Rules.RulesMax.validate(value, rules[rule]?.value) ? '' : Rules.RulesMax.message(rules[rule]?.message ? rules[rule]?.message : 'vary_large')
                        hasErrors = error !== ''
                        break
                    case 'email':
                        error = Rules.RulesEmail.validate(value) ? '' : Rules.RulesMax.message(rules[rule]?.message ? rules[rule]?.message : 'email_incorrect')
                        hasErrors = error !== ''
                        break
                    case 'number':
                        error = Rules.RulesNumber.validate(value)
                            ? ''
                            : Rules.RulesNumber.message(rules[rule]?.message
                                ? rules[rule]?.message
                                : 'number_incorrect'
                            )
                        hasErrors = error !== ''
                        break

                    default:
                        break
                }
            }
        }
        return error
    }
}

export default new RulesLib()