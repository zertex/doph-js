import RulesUrl from "@/libs/rules/rules.url.js";
import RulesRequired from "@/libs/rules/rules.required.js";
import RulesMin from "@/libs/rules/rules.length.min.js";
import RulesMax from "@/libs/rules/rules.length.max.js";
import RulesEmail from "@/libs/rules/rules.email.js";
import RulesNumber from "@/libs/rules/rules.number.js";

export default {
    RulesUrl,
    RulesRequired,
    RulesMin,
    RulesMax,
    RulesEmail,
    RulesNumber
}
