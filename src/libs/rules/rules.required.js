class RulesRequired {
    validate(str = '') {
        return str !== '';
    }

    message(text) {
        return text
    }
}

export default new RulesRequired()