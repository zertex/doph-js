import {openDialog} from "vue3-promise-dialog";
import DConfirmDialog from "@/components/DModal/DConfirmDialog.vue";

export async function confirm(title, message) {
    return await openDialog(DConfirmDialog, {title, message});
}
