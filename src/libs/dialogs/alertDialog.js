import {openDialog} from "vue3-promise-dialog";
import DAlertDialog from "@/components/DModal/DAlertDialog.vue";

export async function alert(title, message) {
    return await openDialog(DAlertDialog, {title, message});
}
