import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import clickOutside from './directives/click-outside.js'
import touch from './directives/touch.js'
import keydown from './directives/keydown.js'
//import vueTouchEvents from "@/directives/touch-events.js";
import './scss/variables.scss'
import './scss/components.scss'
import './scss/themes.scss'
import './scss/global.scss'
import './scss/grid/grid.scss'

import router from "./router"
import disableContext from "@/directives/disable-context.js";

const app = createApp(App)

app.use(router)
//app.use(vueTouchEvents)
app.directive("click-outside", clickOutside)
app.directive("touch", touch)
app.directive("keydown", keydown)
app.directive("disable-context", disableContext)

app.mount('#app')

