//import DButton from "./components/DButton";
// import DLabel from "@/components/DLabel.vue";
// import DFufa from "@/components/DFufa.vue";
// import DMods from "@/components/DMods.vue";

import './scss/variables.scss'
import './scss/components.scss'
import './scss/themes.scss'
import './scss/global.scss'
import './scss/grid/grid.scss'
//import './scss/fonts.scss'


import * as components from './components'

// const DophLibrary = {
//     install(app) {
//         // Auto import all components
//         for (const componentKey in components) {
//             app.use((components)[componentKey])
//         }
//     }
// }

//export default DophLibrary

// export all components as vue plugin

export * from './components'
//export { DButton }

//export { DButton, DLabel, DMods, DFufa }
//
// const plugins = [
//     DButton, DLabel, DMods, DFufa
// ]
//
// export function installComponents(app) {
//     plugins.forEach((plugin) => app.use(plugin));
// }

// const plugin = {
//     install (Vue) {
//         Vue.component('DButton', DButton)
//         Vue.component('DMods', DMods)
//         // for (const prop in components) {
//         //     if (components.hasOwnProperty(prop)) {
//         //         const component = components[prop]
//         //         Vue.component(component.name, component)
//         //     }
//         // }
//     }
// }
//
// export default plugin


// import * as components from './components';
// //import * as directives from './directives';
//
// export * from './components';
//
// const install = (Vue) => {
//     //if (install.installed) return;
//
//     const _components = Object.keys(components).map(
//         (key) => components[key]
//     )
//
//     //const _directives = Object.keys(directives).map(
//     //    (key) => directives[key as keyof typeof directives]
//     //)
//
//     _components.forEach((component) => {
//         if (
//             component.hasOwnProperty('name') ||
//             component.hasOwnProperty('__name')
//         ) {
//             Vue.component(`${component.name || component.__name}`, component);
//         }
//     });
//
//     // _directives.forEach((directive: any) => {
//     //     if (directive.hasOwnProperty('install')) {
//     //         Vue.use(directive);
//     //     } else if (directive.hasOwnProperty('name')) {
//     //         window[directive.name] = directive;
//     //         Vue.config.globalProperties[directive.name] = directive;
//     //     }
//     // });
// }
//
// export default {
//     install,
// };


//
// import components from'./components'
//
// const plugin = {
//     install (Vue) {
//         for (const prop in components) {
//             if (components.hasOwnProperty(prop)) {
//                 const component = components[prop]
//                 Vue.component(component.name, component)
//             }
//         }
//     }
// }
//
// export default plugin