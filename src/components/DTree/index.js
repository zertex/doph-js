import DTree from "./DTree.vue";
import DTreeItem from "./DTreeItem.vue"

export default {
    install(Vue) {
        Vue.component('DTree', DTree)
        Vue.component('DTreeItem', DTreeItem)
    }
}

export { DTree, DTreeItem }