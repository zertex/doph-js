import DButton from "./DButton.vue";

export default {
    install(Vue) {
        Vue.component('DButton', DButton)
    }
}

//export { DButton }