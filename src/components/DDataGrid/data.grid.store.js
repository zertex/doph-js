import { reactive } from 'vue'

const DataGridStore = ({
    state: reactive({}),

    state2: reactive({
        items: [],
        sortField: '',
        sortDirection: '',
        selected: []
    }),
    getters: {
        getItems(id) {
            //return v
            let tempList = DataGridStore.state[id].items.slice()

            //let tempCards = []
            // if (state.cardSearch) {
            //     tempCards = state.cardsList.filter((item) => {
            //         return (item.name && item.name.toUpperCase().includes(state.cardSearch.toUpperCase())) ||
            //             (item.lastname && item.lastname.toUpperCase().includes(state.cardSearch.toUpperCase())) ||
            //             (item.title && item.title.toUpperCase().includes(state.cardSearch.toUpperCase())) ||
            //             (item.job && item.job.toUpperCase().includes(state.cardSearch.toUpperCase()))
            //     })
            // } else {
            //     tempCards = state.cardsList.slice()
            // }
            if (DataGridStore.state[id].sortField && DataGridStore.state[id].sortDirection === 'asc') {
                tempList.sort((a, b) => a[DataGridStore.state[id].sortField] !== null && b[DataGridStore.state[id].sortField] !== null
                    ? String(a[DataGridStore.state[id].sortField]).localeCompare(String(b[DataGridStore.state[id].sortField]))
                    : null)
            } else if (DataGridStore.state[id].sortField && DataGridStore.state[id].sortDirection === 'desc') {
                tempList.sort((a, b) => a[DataGridStore.state[id].sortField] !== null && b[DataGridStore.state[id].sortField] !== null
                    ? String(b[DataGridStore.state[id].sortField]).localeCompare(String(a[DataGridStore.state[id].sortField]))
                    : null)
            }
            return tempList
        },
        getSortField(id) {
            return DataGridStore.state[id].sortField
        },
        getSortDirection(id) {
            return DataGridStore.state[id].sortDirection
        },
        getSelected(id) {
            return DataGridStore.state[id].selected
        }
    },
    mutations: {
        addDataGrid(id) {
            DataGridStore.state[id] = {
                items: [],
                sortField: '',
                sortDirection: '',
                selected: []
            }
        },
        setItems(id, items) {
            DataGridStore.state[id].items = items
        },
        setSortField(id, sort) {
            DataGridStore.state[id].sortField = sort
        },
        setSortDirection(id, dir) {
            DataGridStore.state[id].sortDirection = dir
        },
        select(id, selected) {
            DataGridStore.state[id].selected = [selected]
        },
        addSelect(id, selected) {
            if (!DataGridStore.state[id].selected.includes(selected)) {
                DataGridStore.state[id].selected.push(selected)
            }
        },
        unSelect(id, selected) {
            if (DataGridStore.state[id].selected.includes(selected)) {
                const index = DataGridStore.state[id].selected.indexOf(selected);
                if (index !== -1) {
                    DataGridStore.state[id].selected.splice(index, 1);
                }
            }
        },
        setSelect(id, selection) {
            DataGridStore.state[id].selected = selection
        }
    },
    actions: {
        selectAll(id) {
            for (let i = 0; i < DataGridStore.state[id].items.length; i++) {
                //DataGridStore.state.selected[DataGridStore.state.items[i].key] = DataGridStore.state.items[i]
                //DataGridStore.state.selected[DataGridStore.state.items[i].id] = DataGridStore.state.items[i].id
                DataGridStore.state[id].selected.push(DataGridStore.state[id].items[i].id)
            }
        },
        unselectAll(id) {
            DataGridStore.state[id].selected = []
        }
    }
})

export default DataGridStore
