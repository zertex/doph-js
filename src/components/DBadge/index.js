import DBadge from "./DBadge.vue";

export default {
    install(Vue) {
        Vue.component('DBadge', DBadge)
    }
}

export { DBadge }