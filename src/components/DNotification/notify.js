import emitter from '../../libs/emitter.ts'

export const notify = (args) => {
    if (typeof args === 'string') {
        args = { title: '', text: args }
    }

    if (typeof args === 'object') {
        emitter.emit('notification-add', args)
    }
}

notify.close = (id) => {
    emitter.emit('notification-close', id)
}

export const useNotification = () => {
    return { notify }
}
