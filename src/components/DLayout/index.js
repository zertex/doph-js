import DLayoutAdmin from "./DLayoutAdmin.vue";
import DLayoutContent from "./DLayoutContent.vue"
import DLayoutFooter from "./DLayoutFooter.vue"
import DLayoutHeader from "./DLayoutHeader.vue"
import DLayoutSidebar from "./DLayoutSidebar.vue"
import DLayoutTab from "./DLayoutTab.vue"

export default {
    install(Vue) {
        Vue.component('DLayoutAdmin', DLayoutAdmin)
        Vue.component('DLayoutContent', DLayoutContent)
        Vue.component('DLayoutFooter', DLayoutFooter)
        Vue.component('DLayoutHeader', DLayoutHeader)
        Vue.component('DLayoutSidebar', DLayoutSidebar)
        Vue.component('DLayoutTab', DLayoutTab)
    }
}

export { DLayoutAdmin, DLayoutContent, DLayoutFooter, DLayoutHeader, DLayoutSidebar, DLayoutTab}