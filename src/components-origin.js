import DButton from "./components/DButton.vue";
import DLabel from "@/components/DLabel.vue";
import DMods from "@/components/DMods.vue";

import './scss/variables.scss'
import './scss/components.scss'
import './scss/themes.scss'
import './scss/global.scss'

export default { DButton, DLabel, DMods }