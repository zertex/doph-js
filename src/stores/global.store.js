import {reactive} from "vue";

const DGlobalStore = ({
    state: reactive({
        theme: 'dark',
    }),
    getters: {
        theme() {
            return DGlobalStore.state.theme
        },
    },
    mutations: {
        setTheme(theme) {
            DGlobalStore.state.theme = theme
            // disable themes
            document.documentElement.classList.remove('dark');
            document.documentElement.classList.remove('light');
            // set theme
            document.documentElement.classList.add(theme);
        },
    },
    actions: {
        toggleTheme() {
            DGlobalStore.state.theme = DGlobalStore.state.theme === 'dark' ? 'light' : 'dark'
            // disable themes
            document.documentElement.classList.remove('dark');
            document.documentElement.classList.remove('light');
            // set theme
            document.documentElement.classList.add(DGlobalStore.state.theme);
        },
    }
})

export default DGlobalStore
