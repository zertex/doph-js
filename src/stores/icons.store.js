import { reactive } from 'vue'

const DIconStore = ({
    state: {},
    getters: {
        getIcon(name) {
            return DIconStore.state[name]
        },
        getIcons() {
            return DIconStore.state
        }
    },
    mutations: {
        addIcon(name, component) {
            DIconStore.state['name'] = component
        },
        addIcons(icons) {
            for (const key in icons) {
                if (icons.hasOwnProperty(key)) {
                    DIconStore.state[key] = icons[key]
                }
            }
        }
    }
})

export default DIconStore