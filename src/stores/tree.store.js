import { reactive } from 'vue'
import emitter from '@/libs/emitter.ts'

const DTreeStore = ({
    state: reactive({
        trees: {}
    }),
    getters: {
        getItems(treeId) {
            return DTreeStore.state.trees[treeId].items
        },
        getItemsSelect(treeId) {
            return DTreeStore.state.trees[treeId].selected
        },
        // data - ноды, dataId - ID ноды родителя data
        getParentId(data, dataId, value, key = 'id', sub = 'children', tempObj = {}) {
            if (value && data) {
                data.find((node) => {
                    if (node[key] === value) {
                        tempObj.found = dataId;
                        return node;
                    }
                    return DTreeStore.getters.getParentId(node[sub], node.id, value, key, sub, tempObj);
                });
                if (tempObj.found) {
                    return tempObj.found;
                }
            }
            return false;
        },
    },
    mutations: {
        setTreeItems(treeId, tree) {
            DTreeStore.state.trees[treeId].items = tree
        },
        setItemSelect(treeId, node) {
            //emitter.emit(`select-${treeId}`, {id:treeId, model:node})

            if (DTreeStore.state.trees[treeId]['noSelect']) {
                return
            }

            if (DTreeStore.state.trees[treeId]['multiSelect']) {
                if (DTreeStore.state.trees[treeId].selected.includes(node.id)) {
                    DTreeStore.state.trees[treeId].selected = DTreeStore.state.trees[treeId].selected.filter(function(item) { return item !== node.id })
                } else {
                    DTreeStore.state.trees[treeId].selected = [...DTreeStore.state.trees[treeId].selected, ...[node.id]]
                }
            } else {
                DTreeStore.state.trees[treeId]['selected'] = [node.id]
            }
        },
        unSelect(treeId) {
            DTreeStore.state.trees[treeId]['selected'] = []
        }
    },
    actions: {
        initTreeStore(treeId, noSelect, multiSelect, expanded, transfer, renderTitle, beforeItem, dropInside = true) {
            DTreeStore.state.trees[treeId] = []
            DTreeStore.state.trees[treeId]['selected'] = []
            DTreeStore.state.trees[treeId]['noSelect'] = noSelect
            DTreeStore.state.trees[treeId]['multiSelect'] = multiSelect
            DTreeStore.state.trees[treeId]['expanded'] = expanded
            DTreeStore.state.trees[treeId]['transfer'] = transfer
            DTreeStore.state.trees[treeId]['renderTitle'] = renderTitle
            DTreeStore.state.trees[treeId]['beforeItem'] = beforeItem
            DTreeStore.state.trees[treeId]['dropInside'] = dropInside
        },
        renderTitle(treeId, title, item = null) {
            return DTreeStore.state.trees[treeId].renderTitle ? DTreeStore.state.trees[treeId].renderTitle(title, item) : title
        },
        beforeItem(treeId, item) {
            return DTreeStore.state.trees[treeId].beforeItem ? DTreeStore.state.trees[treeId].beforeItem(item) : true
        },
        isItemSelected(treeId, nodeId) {
            return DTreeStore.state.trees[treeId].selected.includes(nodeId)
        },
        toggleItem(treeId, node) {
            node.expanded = node.expanded === undefined || !node.expanded
            const isFolder = node.children && node.children.length
            if (isFolder) {
                if (node.expanded) {
                    emitter.emit(`expand-${treeId}`, {id:treeId, model:node})
                } else {
                    emitter.emit(`collapse-${treeId}`, {id:treeId, model:node})
                }
            }
        },
        isTreeExpanded(treeId) {
            return DTreeStore.state.trees[treeId].expanded
        },
        isChildOfParent(treeId, tree, parentId, childId) {
            const parentNode = DTreeStore.actions.deepSearch(tree, parentId)
            if (parentNode && parentNode.children) {
                for (let i = 0; i < parentNode.children.length; i++) {
                    if (parentNode.children[i].id === childId) {
                        return true
                    }
                }
            }
            return false
        },
        // Является ли нода родителем указанного потомка
        isParentOfChild(treeId, tree, parentId, childId) {
            const parentNode = DTreeStore.actions.deepSearch(tree, parentId)
            if (parentNode && parentNode.children) {
                const childNode = DTreeStore.actions.deepSearch(parentNode.children, childId)
                if (childNode) {
                    return true
                }
            }
            return false
        },
        deepSearch(data, value, key = 'id', sub = 'children', tempObj = {}) {
            if (value && data) {
                data.find((node) => {
                    if (String(node[key]) === String(value)) {
                        tempObj.found = node;
                        return node;
                    }
                    return DTreeStore.actions.deepSearch(node[sub], value, key, sub, tempObj);
                });
                if (tempObj.found) {
                    return tempObj.found;
                }
            }
            return false;
        },
        deepSearchByTitle(data, value, key = 'title', sub = 'children', tempObj = []) {
            if (value && data) {
                data.find((node) => {
                    const pattern = new RegExp(value, 'gi');
                    if (node[key].toString().toLowerCase().match(pattern) !== null) {
                        tempObj.push(node)
                    }
                    DTreeStore.actions.deepSearchByTitle(node[sub], value, key, sub, tempObj)
                })
                if (tempObj) {
                    for (let i = 0; i < tempObj.length; i++) {
                        //delete tempObj[i].children
                    }
                    return tempObj
                }
            }
            return tempObj
        },
        moveTreeNode(fromTreeId, treeId, tree, id, targetId, position, transfer = true) {
            //const movedNode = DTreeStore.actions.deleteTreeNode(tree, id)
            if (transfer) {
                const movedNode = DTreeStore.actions.deleteTreeNode(tree, id)
                DTreeStore.actions.insertTreeNode(tree, targetId, movedNode, position)
                emitter.emit(`move-${treeId}`, {fromTreeId:fromTreeId,  toTreeId:treeId, moved:movedNode?.id, target:targetId, position})
            } else {
                emitter.emit(`move-${treeId}`, {fromTreeId:fromTreeId,  toTreeId:treeId, moved:id, target:targetId, position})
            }
        },
        insertTreeNode(tree, id, node, position) {
            if (tree) {
                for (let i = 0; i < tree.length; i++) {
                    if (tree[i].id === id) {
                        //console.log(position)
                        let idx = i;
                        if (position === 'after') {
                            idx++;
                            tree.splice(idx, 0, node)
                        } else if (position === 'before') {
                            tree.splice(idx, 0, node)
                        } else if (position === 'inside') {
                            if (!Object.hasOwn(tree[i], 'children')) {
                                tree[i].children = []
                                tree[i].opened = true
                            }
                            tree[i].children.splice(0, 0, node)
                        }
                        return tree[i]
                    }
                    const found = this.insertTreeNode(tree[i].children, id, node, position)
                    if (found) return found;
                }
            }
        },
        deleteTreeNode(tree, id) {
            if (tree) {
                for (let i = 0; i < tree.length; i++) {
                    if (tree[i].id === id) {
                        const result = tree[i]
                        tree.splice(i, 1)
                        return result
                    }
                    const found = DTreeStore.actions.deleteTreeNode(tree[i].children, id)
                    if (found) return found;
                }
            }
        }
    }
})

export default DTreeStore
