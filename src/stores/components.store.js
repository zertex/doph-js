import { reactive } from 'vue'

const DComponentStore = ({
    state: {},
    getters: {
        getComponent(name) {
            return DComponentStore.state[name]
        },
        getComponents() {
            return DComponentStore.state
        }
    },
    mutations: {
        addComponent(name, component) {
            DComponentStore.state['name'] = component
        },
        addComponents(components) {
            for (const key in components) {
                if (components.hasOwnProperty(key)) {
                    DComponentStore.state[key] = components[key]
                }
            }
        }
    }
})

export default DComponentStore
