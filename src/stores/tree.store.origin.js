import { reactive } from 'vue'
import emitter from '@/libs/emitter.ts'

const DTreeStore = ({
    state: reactive({
        trees: {}
    }),
    getters: {
        getItems(treeId) {
            return DTreeStore.state.trees[treeId].items
        },
        getItemsSelect(treeId) {
            return DTreeStore.state.trees[treeId].selected
        },
        // data - ноды, dataId - ID ноды родителя data
        getParentId(data, dataId, value, key = 'id', sub = 'children', tempObj = {}) {
            if (value && data) {
                data.find((node) => {
                    if (node[key] === value) {
                        tempObj.found = dataId;
                        return node;
                    }
                    return DTreeStore.getters.getParentId(node[sub], node.id, value, key, sub, tempObj);
                });
                if (tempObj.found) {
                    return tempObj.found;
                }
            }
            return false;
        },
    },
    mutations: {
        setTreeItems(treeId, tree) {
            DTreeStore.state.trees[treeId].items = tree
        },
        setItemSelect(treeId, node) {
            emitter.emit(`select-${treeId}`, {id:treeId, model:node})

            if (DTreeStore.state.trees[treeId]['noSelect']) {
                return
            }

            if (DTreeStore.state.trees[treeId]['multiSelect']) {
                if (DTreeStore.state.trees[treeId].selected.includes(node.id)) {
                    DTreeStore.state.trees[treeId].selected = DTreeStore.state.trees[treeId].selected.filter(function(item) { return item !== node.id })
                } else {
                    DTreeStore.state.trees[treeId].selected = [...DTreeStore.state.trees[treeId].selected, ...[node.id]]
                }
            } else {
                DTreeStore.state.trees[treeId]['selected'] = [node.id]
            }
        }
    },
    actions: {
        initTreeStore(treeId, items, noSelect, multiSelect, expanded, transfer, renderTitle) {
            DTreeStore.state.trees[treeId] = []
            DTreeStore.state.trees[treeId]['selected'] = []
            DTreeStore.state.trees[treeId]['items'] = items //structuredClone(items)
            DTreeStore.state.trees[treeId]['noSelect'] = noSelect
            DTreeStore.state.trees[treeId]['multiSelect'] = multiSelect
            DTreeStore.state.trees[treeId]['expanded'] = expanded
            DTreeStore.state.trees[treeId]['transfer'] = transfer
            DTreeStore.state.trees[treeId]['renderTitle'] = renderTitle
        },
        renderTitle(treeId, title) {
            return DTreeStore.state.trees[treeId].renderTitle ? DTreeStore.state.trees[treeId].renderTitle(title) : title
        },
        isItemSelected(treeId, nodeId) {
            return DTreeStore.state.trees[treeId].selected.includes(nodeId)
        },
        toggleItem(treeId, nodeId) {
            const node = DTreeStore.actions.deepSearch(DTreeStore.state.trees[treeId].items, nodeId)
            node.expanded = node.expanded === undefined || ! node.expanded
            const isFolder = node.children && node.children.length
            if (isFolder) {
                if (node.expanded) {
                    emitter.emit(`expand-${treeId}`, {id:treeId, model:node})
                } else {
                    emitter.emit(`collapse-${treeId}`, {id:treeId, model:node})
                }
            }
        },
        isItemExpanded(treeId, nodeId) {
            const node = DTreeStore.actions.deepSearch(DTreeStore.state.trees[treeId].items, nodeId)
            if (DTreeStore.state.trees[treeId].expanded) {
                node.expanded = node.expanded === undefined ? true : node.expanded
            }
            return node.expanded
        },
        // Является ли нода потомком указанного родителя
        isChildOfParent(treeId, parentId, childId) {
            const parentNode = DTreeStore.actions.deepSearch(DTreeStore.state.trees[treeId].items, parentId)
            if (parentNode && parentNode.children) {
                for (let i = 0; i < parentNode.children.length; i++) {
                    if (parentNode.children[i].id === childId) {
                        return true
                    }
                }
            }
            return false
        },
        // Является ли нода родителем указанного потомка
        isParentOfChild(treeId, parentId, childId) {
            const parentNode = DTreeStore.actions.deepSearch(DTreeStore.state.trees[treeId].items, parentId)
            if (parentNode && parentNode.children) {
                const childNode = DTreeStore.actions.deepSearch(parentNode.children, childId)
                if (childNode) {
                    return true
                }
            }
            return false
        },
        deepSearch(data, value, key = 'id', sub = 'children', tempObj = {}) {
            if (value && data) {
                data.find((node) => {
                    if (node[key] === value) {
                        tempObj.found = node;
                        return node;
                    }
                    return DTreeStore.actions.deepSearch(node[sub], value, key, sub, tempObj);
                });
                if (tempObj.found) {
                    return tempObj.found;
                }
            }
            return false;
        },
        moveTreeNode(fromTreeId, treeId, id, targetId, position) {
            const tree = DTreeStore.getters.getItems(treeId)
            const movedNode = DTreeStore.actions.deleteTreeNode(tree, id)
            DTreeStore.actions.insertTreeNode(tree, targetId, movedNode, position)
            emitter.emit(`move-${treeId}`, {fromTreeId:fromTreeId,  toTreeId:treeId, moved:movedNode.id, target:targetId, position})
        },
        insertTreeNode(tree, id, node, position) {
            if (tree) {
                for (let i = 0; i < tree.length; i++) {
                    if (tree[i].id === id) {
                        //console.log(position)
                        let idx = i;
                        if (position === 'after') {
                            idx++;
                            tree.splice(idx, 0, node)
                        } else if (position === 'before') {
                            tree.splice(idx, 0, node)
                        } else if (position === 'inside') {
                            if (!Object.hasOwn(tree[i], 'children')) {
                                tree[i].children = []
                                tree[i].opened = true
                            }
                            tree[i].children.splice(0, 0, node)
                        }
                        return tree[i]
                    }
                    const found = this.insertTreeNode(tree[i].children, id, node, position)
                    if (found) return found;
                }
            }
        },
        deleteTreeNode(tree, id) {
            if (tree) {
                for (let i = 0; i < tree.length; i++) {
                    if (tree[i].id === id) {
                        const result = tree[i]
                        tree.splice(i, 1)
                        return result
                    }
                    const found = DTreeStore.actions.deleteTreeNode(tree[i].children, id)
                    if (found) return found;
                }
            }
        }
    }
})

export default DTreeStore
