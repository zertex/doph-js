import {reactive} from "vue";

const DScrollStore = ({
    state: reactive({
        body: true,
    }),
    getters: {
        getState() {
            return DScrollStore.state.body
        },
    },
    mutations: {
        setState(value) {
            DScrollStore.state.body = value
        },
    },
    actions: {
        toggleState() {
            DScrollStore.state.body = !DScrollStore.state.body
        },
    }
})

export default DScrollStore