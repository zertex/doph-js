const disableContext = {
    beforeMount(el) {
        el.addEventListener('contextmenu', event => {
            event.preventDefault() // Prevent the default context menu behavior
        })
    }
}

export default disableContext