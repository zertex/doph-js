const focus = {
    beforeMount(el) {
        el.focus()
    }
}

export default focus
