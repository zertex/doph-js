import { ref, computed, watch, onUnmounted } from 'vue';
export function useBoolean(initValue = false) {
    const bool = ref(initValue);
    function setBool(value) {
        bool.value = value;
    }
    function setTrue() {
        setBool(true);
    }
    function setFalse() {
        setBool(false);
    }
    function toggle() {
        setBool(!bool.value);
    }
    return {
        bool,
        setBool,
        setTrue,
        setFalse,
        toggle
    };
}

export function useFixedTransformStyle(isFixed) {
    const scrollLeft = ref(0);
    const transformStyle = computed(() => `transform: translateX(${-scrollLeft.value}px);`);
    let isInit = false;
    function setScrollLeft(sLeft) {
        scrollLeft.value = sLeft;
    }
    function scrollHandler() {
        var _a;
        const sLeft = ((_a = document.scrollingElement) === null || _a === void 0 ? void 0 : _a.scrollLeft) || 0;
        setScrollLeft(sLeft);
    }
    function initScrollLeft() {
        scrollHandler();
    }
    function addScrollEventListener() {
        document.addEventListener('scroll', scrollHandler);
    }
    function removeScrollEventListener() {
        if (!isInit)
            return;
        document.removeEventListener('scroll', scrollHandler);
    }
    function init() {
        initScrollLeft();
        addScrollEventListener();
        isInit = true;
    }
    watch(isFixed, newValue => {
        if (newValue) {
            init();
        }
        else {
            removeScrollEventListener();
        }
    }, { immediate: true });
    onUnmounted(() => {
        removeScrollEventListener();
    });
    return transformStyle;
}
