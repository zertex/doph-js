import { createWebHistory, createRouter } from "vue-router"

import VLayout from "../views/VLayout.vue"
import VIndex from "../views/VIndex.vue"
import VTest from "@/views/VTest.vue";
import VImage from "@/views/VImage.vue";

const routes = [
    {
        path: "/",
        name: "Index",
        component: VIndex,
    },
    {
        path: "/layout",
        name: "Layout",
        component: VLayout,
    },
    {
        path: "/test",
        name: "Test",
        component: VTest,
    },
    {
        path: "/img",
        name: "Img",
        component: VImage,
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router