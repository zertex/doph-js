# DophJS

Простая библиотека компонентов интерфейса для Vue3

[![Npm package total downloads](https://badgen.net/npm/dt/doph-js)](https://www.npmjs.com/package/doph-js)
[![made-with-javascript](https://img.shields.io/badge/Made%20with-JavaScript-1f425f.svg)](https://www.javascript.com)
[![Npm package version](https://badgen.net/npm/v/doph-js)](https://www.npmjs.com/package/doph-js)
[![Npm package dependents](https://badgen.net/npm/dependents/doph-js)](https://npmjs.com/package/doph-js)
[![Npm package license](https://badgen.net/npm/license/doph-js)](https://npmjs.com/package/doph-js)

## [Документация](https://js.doph.ru)