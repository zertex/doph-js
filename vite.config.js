import vue from '@vitejs/plugin-vue'
import * as path from "path";
import { defineConfig } from 'vite'
import dts from "vite-plugin-dts";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    dts(),
    // VitePluginFonts({
    //   custom: {
    //     families: [
    //       {
    //         name: 'Open Sans',
    //         local: 'Open Sans',
    //         src: './src/assets/fonts/*.woff',
    //       }
    //     ],
    //
    //     display: 'auto',
    //     preload: true,
    //     prefetch: false,
    //     injectTo: 'head-prepend',
    //   }
    // }),
  ],
  build: {
    lib: {
      entry: path.resolve(__dirname, "src/index.js"),
      name: "DophJs",
      fileName: "dophJs",
    },
    rollupOptions: {
      external: ["vue"],
      output: {
        globals: {
          vue: "Vue",
        },
      },
    },
  },
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src")
    }
  }
});
