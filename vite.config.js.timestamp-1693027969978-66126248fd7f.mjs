// vite.config.js
import vue from "file:///D:/projects/dophjs/doph-js/node_modules/@vitejs/plugin-vue/dist/index.mjs";
import * as path from "path";
import { defineConfig } from "file:///D:/projects/dophjs/doph-js/node_modules/vite/dist/node/index.js";
import dts from "file:///D:/projects/dophjs/doph-js/node_modules/vite-plugin-dts/dist/index.mjs";
import { VitePluginFonts } from "file:///D:/projects/dophjs/doph-js/node_modules/vite-plugin-fonts/dist/index.js";
var __vite_injected_original_dirname = "D:\\projects\\dophjs\\doph-js";
var vite_config_default = defineConfig({
  plugins: [
    vue(),
    dts()
    // VitePluginFonts({
    //   custom: {
    //     families: [
    //       {
    //         name: 'Open Sans',
    //         local: 'Open Sans',
    //         src: './src/assets/fonts/*.woff',
    //       }
    //     ],
    //
    //     display: 'auto',
    //     preload: true,
    //     prefetch: false,
    //     injectTo: 'head-prepend',
    //   }
    // }),
  ],
  build: {
    lib: {
      entry: path.resolve(__vite_injected_original_dirname, "src/index.js"),
      name: "DophJs",
      fileName: "dophJs"
    },
    rollupOptions: {
      external: ["vue"],
      output: {
        globals: {
          vue: "Vue"
        }
      }
    }
  },
  resolve: {
    alias: {
      "@": path.resolve(__vite_injected_original_dirname, "src")
    }
  }
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcuanMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCJEOlxcXFxwcm9qZWN0c1xcXFxkb3BoanNcXFxcZG9waC1qc1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9maWxlbmFtZSA9IFwiRDpcXFxccHJvamVjdHNcXFxcZG9waGpzXFxcXGRvcGgtanNcXFxcdml0ZS5jb25maWcuanNcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfaW1wb3J0X21ldGFfdXJsID0gXCJmaWxlOi8vL0Q6L3Byb2plY3RzL2RvcGhqcy9kb3BoLWpzL3ZpdGUuY29uZmlnLmpzXCI7aW1wb3J0IHZ1ZSBmcm9tICdAdml0ZWpzL3BsdWdpbi12dWUnXG5pbXBvcnQgKiBhcyBwYXRoIGZyb20gXCJwYXRoXCI7XG5pbXBvcnQgeyBkZWZpbmVDb25maWcgfSBmcm9tICd2aXRlJ1xuaW1wb3J0IGR0cyBmcm9tIFwidml0ZS1wbHVnaW4tZHRzXCI7XG5pbXBvcnQgeyBWaXRlUGx1Z2luRm9udHMgfSBmcm9tICd2aXRlLXBsdWdpbi1mb250cydcblxuLy8gaHR0cHM6Ly92aXRlanMuZGV2L2NvbmZpZy9cbmV4cG9ydCBkZWZhdWx0IGRlZmluZUNvbmZpZyh7XG4gIHBsdWdpbnM6IFtcbiAgICB2dWUoKSxcbiAgICBkdHMoKSxcbiAgICAvLyBWaXRlUGx1Z2luRm9udHMoe1xuICAgIC8vICAgY3VzdG9tOiB7XG4gICAgLy8gICAgIGZhbWlsaWVzOiBbXG4gICAgLy8gICAgICAge1xuICAgIC8vICAgICAgICAgbmFtZTogJ09wZW4gU2FucycsXG4gICAgLy8gICAgICAgICBsb2NhbDogJ09wZW4gU2FucycsXG4gICAgLy8gICAgICAgICBzcmM6ICcuL3NyYy9hc3NldHMvZm9udHMvKi53b2ZmJyxcbiAgICAvLyAgICAgICB9XG4gICAgLy8gICAgIF0sXG4gICAgLy9cbiAgICAvLyAgICAgZGlzcGxheTogJ2F1dG8nLFxuICAgIC8vICAgICBwcmVsb2FkOiB0cnVlLFxuICAgIC8vICAgICBwcmVmZXRjaDogZmFsc2UsXG4gICAgLy8gICAgIGluamVjdFRvOiAnaGVhZC1wcmVwZW5kJyxcbiAgICAvLyAgIH1cbiAgICAvLyB9KSxcbiAgXSxcbiAgYnVpbGQ6IHtcbiAgICBsaWI6IHtcbiAgICAgIGVudHJ5OiBwYXRoLnJlc29sdmUoX19kaXJuYW1lLCBcInNyYy9pbmRleC5qc1wiKSxcbiAgICAgIG5hbWU6IFwiRG9waEpzXCIsXG4gICAgICBmaWxlTmFtZTogXCJkb3BoSnNcIixcbiAgICB9LFxuICAgIHJvbGx1cE9wdGlvbnM6IHtcbiAgICAgIGV4dGVybmFsOiBbXCJ2dWVcIl0sXG4gICAgICBvdXRwdXQ6IHtcbiAgICAgICAgZ2xvYmFsczoge1xuICAgICAgICAgIHZ1ZTogXCJWdWVcIixcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgfSxcbiAgfSxcbiAgcmVzb2x2ZToge1xuICAgIGFsaWFzOiB7XG4gICAgICBcIkBcIjogcGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgXCJzcmNcIilcbiAgICB9XG4gIH1cbn0pO1xuIl0sCiAgIm1hcHBpbmdzIjogIjtBQUF3USxPQUFPLFNBQVM7QUFDeFIsWUFBWSxVQUFVO0FBQ3RCLFNBQVMsb0JBQW9CO0FBQzdCLE9BQU8sU0FBUztBQUNoQixTQUFTLHVCQUF1QjtBQUpoQyxJQUFNLG1DQUFtQztBQU96QyxJQUFPLHNCQUFRLGFBQWE7QUFBQSxFQUMxQixTQUFTO0FBQUEsSUFDUCxJQUFJO0FBQUEsSUFDSixJQUFJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxFQWlCTjtBQUFBLEVBQ0EsT0FBTztBQUFBLElBQ0wsS0FBSztBQUFBLE1BQ0gsT0FBWSxhQUFRLGtDQUFXLGNBQWM7QUFBQSxNQUM3QyxNQUFNO0FBQUEsTUFDTixVQUFVO0FBQUEsSUFDWjtBQUFBLElBQ0EsZUFBZTtBQUFBLE1BQ2IsVUFBVSxDQUFDLEtBQUs7QUFBQSxNQUNoQixRQUFRO0FBQUEsUUFDTixTQUFTO0FBQUEsVUFDUCxLQUFLO0FBQUEsUUFDUDtBQUFBLE1BQ0Y7QUFBQSxJQUNGO0FBQUEsRUFDRjtBQUFBLEVBQ0EsU0FBUztBQUFBLElBQ1AsT0FBTztBQUFBLE1BQ0wsS0FBVSxhQUFRLGtDQUFXLEtBQUs7QUFBQSxJQUNwQztBQUFBLEVBQ0Y7QUFDRixDQUFDOyIsCiAgIm5hbWVzIjogW10KfQo=
